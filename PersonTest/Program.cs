﻿using System;

namespace PersonTest
{
    // Define a public class which need te be used in Main
    public class Person
    {

        private string lastname;
        public string Lastname { get { return lastname; } set { lastname = value; } }

        // We could define Property directly
        public string Firstname { get; set; }

        public string Birthdate { get; set; }

        // Constructor by default
        public Person()
        {
            Firstname = "David";
            Lastname = "WANG";
            Birthdate = "01/01/1990";
        }

        // Constructor with parameter list
        public Person(string firstname, string lastname, string birthdate)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Birthdate = birthdate;

        }

        // Destructor
        ~Person() { Console.WriteLine("Destructor of class Person is called."); }

        // This method could be overrided in derived class
        public virtual void Display()
        {
            Console.WriteLine("Object Person displays: ");
            Console.WriteLine(this.Firstname);
            Console.WriteLine(this.Lastname);
            Console.WriteLine(this.Birthdate);

        }
    }


    public class Employe: Person
    {
        public double Salary { get; set; }

        public Employe() => Salary = 1000;

        public Employe(double salary)
        {
            Salary = salary;
        }

        // Destructor
        ~Employe() { Console.WriteLine("Destructor of class Employe is called."); }


        // Override virtual method Display()
        public override void Display()
        {
            //base.Display();
            Console.WriteLine("Object Employe displays: ");
            Console.WriteLine(this.Firstname);
            Console.WriteLine(this.Lastname);
            Console.WriteLine(this.Birthdate);
            Console.WriteLine(this.Salary);
        }

    }

    public class Manager: Employe
    {

        public Manager() => Service = "IT";

        public Manager(string service)
        {
            Service = service;
        }

        // Destructor
        ~Manager() { Console.WriteLine("Destructor of class Manager is called."); }


        public string Service { get; set; }

        // Override virtual method Display()
        public override void Display()
        {
            //base.Display();
            Console.WriteLine("Object Manager displays: ");
            Console.WriteLine(Firstname);
            Console.WriteLine(Lastname);
            Console.WriteLine(Birthdate);
            Console.WriteLine(Salary);
            Console.WriteLine(Service);
        }

    }

    public class ManagerDirector: Manager
    {
        public ManagerDirector() => Company = "Amazon";

        public ManagerDirector(string service, string company) : base(service)
        {
            Company = company;
        }

        public ManagerDirector(string company)
        {
            Company = company;
        }

        // Destructor
        ~ManagerDirector() { Console.WriteLine("Destructor of class ManagerDirector is called."); }

        public string Company { get; set; }

        // Override virtual method Display()
        public override void Display()
        {
            //base.Display();
            Console.WriteLine("Object ManagerDirector displays: ");
            Console.WriteLine(Firstname);
            Console.WriteLine(Lastname);
            Console.WriteLine(Birthdate);
            Console.WriteLine(Salary);
            Console.WriteLine(Service);
            Console.WriteLine(Company);
        }

    }

    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Person myEmploye = new Employe();
            myEmploye.Display();

            myEmploye.Firstname = "Patrick";
            myEmploye.Lastname = "Gibert";
            myEmploye.Birthdate = "02/09/1982";

            //Why not?
            //myEmploye.Salary = 1500;

            myEmploye.Display();

            Employe yourEmploye = new Employe();
            yourEmploye.Display();
            yourEmploye.Salary = 1200;
            yourEmploye.Display();

            ManagerDirector myMD = new ManagerDirector();
            myMD.Display();

            Person[] myTab = new Person[7];

            for(int i = 0; i <4; i++)
            {
                Console.WriteLine("Good");
                Console.WriteLine("Second Commit");
            }


        }
    }
}
